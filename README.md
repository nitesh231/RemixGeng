[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://github.com/niteshraj2310/RemixGeng)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/a85304953a4c470390ec777f6931e55d)](https://app.codacy.com/manual/niteshraj2310/RemixGeng?utm_source=github.com&utm_medium=referral&utm_content=niteshraj2310/RemixGeng&utm_campaign=Badge_Grade_Dashboard)
[![Build Status](https://img.shields.io/travis/niteshraj2310/RemixGeng.svg?label=Travis%20CI&logo=travis&style=flat-square)](https://travis-ci.com/niteshraj2310/RemixGeng)&nbsp;
<a href="https://github.com/niteshraj2310/RemixGeng/commits/sql-extended"><img src="https://img.shields.io/github/last-commit/niteshraj2310/RemixGeng/sql-extended?label=Last%20Commit&style=flat-square&logo=github&color=Green" alt="Commit" /></a>
<a href="https://github.com/niteshraj2310/RemixGeng/graphs/contributors"><img src="https://img.shields.io/github/contributors-anon/niteshraj2310/RemixGeng?label=Contributors&style=flat-square&logo=github&color=Orange" alt="Contributors" /></a>
![Forks](https://img.shields.io/github/forks/sahyam2019/oub-remix)
<a href="https://t.me/Nitesh_231"><img src="https://img.shields.io/badge/-Contact%20Meh🔥😂-black.svg?logo=WhatsApp"> </a>
<a href="https://github.com/niteshraj2310/RemixGeng/watchers"><img src="https://img.shields.io/github/watchers/niteshraj2310/RemixGeng?label=Watch&style=flat-square&logo=github&color=violet" alt="Watch" /></a>
[![Requirements Status](https://requires.io/github/niteshraj2310/RemixGeng/requirements.svg?branch=sql-extended)](https://requires.io/github/niteshraj2310/RemixGeng/requirements/?branch=sql-extended)
<a href="https://deepsource.io/gh/niteshraj2310/RemixGeng/?ref=repository-badge" target="_blank"><img alt="DeepSource" title="DeepSource" src="https://static.deepsource.io/deepsource-badge-light-mini.svg"></a>
[![codebeat badge](https://codebeat.co/badges/049f1ac3-dfc2-4ee6-93a3-e40c1569313f)](https://codebeat.co/projects/github-com-niteshraj2310-remixgeng-sql-extended)
[![Build status](https://ci.appveyor.com/api/projects/status/9h1i5ng3bjqgq11j?svg=true)](https://ci.appveyor.com/project/niteshraj2310/remixgeng-w0cvp)
![FailedChecker](https://github.com/niteshraj2310/RemixGeng/workflows/FailedChecker/badge.svg?branch=sql-extended)
![CI](https://github.com/niteshraj2310/RemixGeng/workflows/CI/badge.svg)
![CodeQL](https://github.com/niteshraj2310/RemixGeng/workflows/CodeQL/badge.svg?branch=sql-extended)
![PyLint](https://github.com/niteshraj2310/RemixGeng/workflows/PyLint/badge.svg)
<a herf="https://hub.docker.com/r/nitesh231/groovy"><img src="https://img.shields.io/docker/image-size/nitesh231/docker/groovy?color=green&label=Docker%20Size&style=for-the-badge&logo=docker&logoColor=white" alt="docker" /></a></br>

# Project 尺 乇 从 工 乂 厶 乇 𠘨 厶

![logo](https://telegra.ph/file/581d9d1e56d67aab89a14.jpg)

```
#include <std/disclaimer.h>
/** 
    Your Telegram account may get banned.
    I am not responsible for any improper use of this bot
    This bot is intended for the purpose of having fun with memes,
    as well as efficiently managing groups.
    You ended up spamming groups, getting reported left and right,
    and you ended up in a Finale Battle with Telegram and at the end
    Telegram Team deleted your account?
    And after that, then you pointed your fingers at us
    for getting your account deleted?
    I will be rolling on the floor laughing at you.
    No personal support will be provided/I won't spoonfeed. If you need help ask in 
    support group mentioned below or ask your friend.
    Open issues if you think there's a bug/problem.
    Open pr if you think you can improve the existing code.

/**
```
## StäyRēmîXD

## How To Host?

The easiest way to deploy this great bot! is click on button below
Make sure you have an account of heroku and follow all the steps required.

Deploy to Heroku:
<p align="left"><a href="https://heroku.com/deploy?template=https://github.com/niteshraj2310/RemixGeng/tree/sql-extended"> <img src="https://www.herokucdn.com/deploy/button.svg" alt="Deploy to Heroku" /></a></p>

## Groups and support

If you want new features, or announcements, you can follow our [OpenUserBot Channel](https://t.me/PaperplaneExtended_news).

For discussion, bug reporting, and help, you can join [OpenUserBot Support Group](https://t.me/PPE_Support).

## How to setup Google Drive
[![SetGDRIVE](https://telegra.ph/file/fde15d05e4bde3448b01a.png)](https://telegra.ph/How-To-Setup-Google-Drive-04-03)

# Credits

Thanks for all : 
* [Baalaji maestro (RaphielGang)](https://github.com/RaphielGang) - Telegram-Paperplane
* [AvinashReddy3108](https://github.com/AvinashReddy3108) - PaperplaneExtended
* [Mkaraniya](https://github.com/mkaraniya) - OpenUserBot
* [DevPatel](https://github.com/Devp73) - OpenUserBot
* [kandnub](https://github.com/kandnub) - TG-UserBot
* [༺αиυвιѕ༻](https://github.com/Dark-Princ3) - X-tra-Telegram
* [MoveAngel](https://github.com/MoveAngel) - One4uBot
* [keselekpermen69](https://github.com/keselekpermen69) - UserButt
* [GengKapak](https://github.com/GengKapak) - DCLXVI and many more people who aren't mentioned here, but may be found in [Contributors](https://github.com/sahyam2019/oub-remix/graphs/contributors).



## License

This userbot licensed on [Raphielscape Public License](https://github.com/niteshraj2310/RemixGeng/blob/sql-extended/LICENSE.md) - Version 1.d, February 2020
